# ESP32 simple Worker Node

This simple source code represents a draft of a possible worker entity in a distributed system composed by **ESP32** boards which expose **WebThing** interfaces. This code is **related** to the [python-load-balancer](https://gitlab.com/flepa/python-load-balancer) repository, together they form the **whole PYTHON LOAD BALANCER project**.

Basically, the worker node can execute, after a **user's request** through the **WebThing** interface, one of its predefined functions (**tasks**). During the execution, the worker node publishes the **operations/s** produced each second through the **MQTT** protocol.

The worker node receives a request as a **JSON** document which has to be **decoded**, the **JSON** contains the following information:

- The **number** of the **task** to execute;

- A **custom value** for the chosen **task**;

- The **minimum operations/s** requested by the **user**;

## Structure

Basically the code has **8 sections**:

1. Setup of the MQTT client for the worker node;

2. Setup of the Wi-Fi client for the worker node;

3. Setup of the WebThing interface of the worker node;

4. Declaration of the **tasks** which the worker node can execute;

5. Definition of the global variables needed for the code execution (`OPS_UNIT` is a constant which defines the total number of operations/s produced by the worker);

6. `setup` function definition;

7. `loop` function definition;

8. **tasks** definition 
