/**
 * Simple server compliant with Mozilla's proposed WoT API
 * Originally based on the HelloServer example
 * Tested on ESP8266, ESP32, Arduino boards with WINC1500 modules (shields or
 * MKR1000)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <Arduino.h>

#include "Thing.h"

#include "WebThingAdapter.h"

#include "Adafruit_MQTT.h"

#include "Adafruit_MQTT_Client.h"

#include <math.h>

/**** MQTT ****/
#define AIO_SERVER "<MQTT Broker IP Address>"
#define AIO_SERVERPORT 1883 // use 8883 for SSL

WiFiClient client;
// or... use WiFiClientSecure for SSL
//WiFiClientSecure client;

Adafruit_MQTT_Client mqtt(&client, AIO_SERVER, AIO_SERVERPORT);

// node_publish will be used to publish the IP address of the node
Adafruit_MQTT_Publish node_publish = Adafruit_MQTT_Publish(&mqtt, "devices");

// result_publish will be used to publish the results produced by the node
Adafruit_MQTT_Publish result_publish = Adafruit_MQTT_Publish(&mqtt, "results");

void MQTT_connect();

/**** END MQTT ****/

/**** WiFi ****/
// secrets.h contains the sensitive data about WiFi authentication
#include "secrets.h"

/**** END WiFi ****/

/**** WebThing ****/
WebThingAdapter *adapter;

const char *taskTypes[] = {
    "Task-1",
    "Task-2",
    "Task-3",
    nullptr
};
ThingDevice worker("task-execution", "Worker Node Task Execution", taskTypes);
ThingProperty task(
    "task",
    "Ask the node to execute task 1-2-3 and provide a configuration",
    STRING,
    "Execute task 1-2-3"
);

/**** END WebThing ****/

/**** Working functions ****/
bool is_prime(int number);

void prime_num_generator(int &number, int up_bound, int &ops_counter);

void cube_of_millis(unsigned long current_time, int custom_value, int &ops_counter);

void factorial(int custom_value, int &ops_counter);

/**** END Working functions ****/

# define OPS_UNIT 50

int task_number, custom_value, ops_counter, ops_value;
unsigned long current_time, current_second;

DynamicJsonDocument conf_json(1024);
String *conf_ptr;
String tmp_str;
char conf_char[256];

String last_text = "No operations!";

bool retrieve_json(const String &str) {
    if (str.charAt(0) != '{' && str.charAt(str.length() - 1) != '}')
        return false;
    
    str.toCharArray(conf_char, 256);
    deserializeJson(conf_json, conf_char);
    return true;
}


void setup(void) {
    Serial.begin(115200);
    Serial.println("");
    Serial.print("Connecting to \"");
    Serial.print(ssid);
    #if defined(ESP8266) || defined(ESP32)
    WiFi.mode(WIFI_STA);
    #endif
    WiFi.begin(ssid, password);

    // Wait for connection
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }

    Serial.println("");
    Serial.print("Connected to ");
    Serial.print(ssid);
    Serial.print(". IP address: ");
    Serial.println(WiFi.localIP());
    adapter = new WebThingAdapter("worker", WiFi.localIP());

    ThingPropertyValue value;
    value.string = &last_text;
    task.setValue(value);
    worker.addProperty(&task);
    
    adapter -> addDevice(&worker);
    adapter -> begin();
    
    Serial.print("HTTP server started. ");
    Serial.print("http://");
    Serial.print(WiFi.localIP());
    Serial.print("/things/");
    Serial.println(worker.id);

    Serial.println("Registering to MQTT Broker");
    DynamicJsonDocument doc(1024);
    doc["name"] = "worker";
    doc["modelURI"] = "http://worker.node";
    doc["ip_address"] = WiFi.localIP();
    char buf[256];
    serializeJson(doc, buf);

    MQTT_connect();

    // Publish our device to be discoverable
    while (!node_publish.publish(buf)) {
        Serial.println(F("Failed, retrying"));
    }
}

void loop(void) {
    MQTT_connect();

    current_time = millis();
    
    adapter -> update();

    if (!retrieve_json(last_text)) {
        Serial.println("Not a JSON format!"); 
        delay(1000);
        return;
    }
    
    task_number = conf_json["task_number"];
    custom_value = conf_json["custom_value"];
    ops_value = conf_json["ops_value"];

    int prime_num = 2;
    ops_counter = 0;
    
    current_second = current_time + 1000;
    while (millis() <= current_second) {
        if (ops_counter >= (ops_value + OPS_UNIT)) {
            Serial.println("Stop working for this second");
            Serial.print("Pause: "); Serial.println(current_second - millis());
            delay(current_second - millis());
            break;
        }
        switch (task_number) {
            case 1:
                prime_num_generator(prime_num, custom_value, ops_counter);
                break;
            case 2:
                cube_of_millis(current_time, custom_value, ops_counter);
                break;
            case 3:
                factorial(custom_value, ops_counter);
                break;
            default:
                Serial.println("Wrong task requested!");
        }
    }
    Serial.print("Ops done: "); Serial.println(ops_counter);
}

void MQTT_connect() {
    int8_t ret;

    // Stop if already connected.
    if (mqtt.connected()) {
        return;
    }

    Serial.print("Connecting to MQTT... ");

    uint8_t retries = 3;
    while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
        Serial.println(mqtt.connectErrorString(ret));
        Serial.println("Retrying MQTT connection in 5 seconds...");
        mqtt.disconnect();
        delay(5000); // wait 5 seconds
        retries--;
        if (retries == 0) {
            // basically die and wait for WDT to reset me
            while (1);
        }
    }
    Serial.println("MQTT Connected!");
}

bool is_prime(int number) {
    for (int i = 2; i * i <= number; i++) {
        if (number % i == 0) {
            return false;
        }
    }
    return true;
}

// TASK 1

void prime_num_generator(int &number, const int up_bound, int &ops_counter) {
    StaticJsonDocument<256> doc;
    char buf[256];

    if (number >= up_bound)
        number = 2;
   
    if (is_prime(number)) {
        Serial.println(number);
        doc["prime_number"] = number;
        doc["ip_address"] = WiFi.localIP();
        serializeJson(doc, buf);
        // Publish our results in the result mqtt topic
        //result_publish.publish(buf);
        while (!result_publish.publish(buf))
            Serial.println(F("Failed, retrying"));
        ops_counter++;
    }
    number++;
}

// TASK 2

void cube_of_millis(unsigned long current_time, int custom_value, int &ops_counter) {
    StaticJsonDocument<256> doc;
    char buf[256];
    unsigned long time_pow;

    time_pow = pow(current_time, custom_value);
    doc["pow"] = time_pow;
    doc["ip_address"] = WiFi.localIP();
    serializeJson(doc, buf);
    // Publish our results in the result mqtt topic
    while (!result_publish.publish(buf))
        Serial.println(F("Failed, retrying"));
    ops_counter++;
}

// TASK 3

void factorial(int custom_value, int &ops_counter) {
    StaticJsonDocument<256> doc;
    char buf[256];
    unsigned long fact = 1;
    const int NUMBER = custom_value;

    for (int i = 1; i <= NUMBER; i++)
        fact *= i;
        
    doc["factorial"] = fact;
    doc["ip_address"] = WiFi.localIP();
    serializeJson(doc, buf);
    // Publish our results in the result mqtt topic
    while (!result_publish.publish(buf))
        Serial.println(F("Failed, retrying"));
    ops_counter++;
}
